// npx cypress open

/*
beforeEach(() => {
    cy.request('http://localhost/employee')
})
*/

it("should open backend", function () {
    cy.request('GET', 'http://localhost/employee').then(
        (response) => {
            // response.body is automatically serialized into JSON
            expect(response.body[0]).to.have.property('firstname', 'Jan') // true
            expect(response.body[0]).to.have.property('lastname', 'Kowalski') // true
            expect(response.body[1]).to.have.property('firstname', 'Anna') // true
            expect(response.body[1]).to.have.property('lastname', 'Nowak') // true
        }
    )
});

it("should open homepage, viit 'Pracownicy' page and add new emplyee to database", function () {
    cy.visit('localhost:4200')

    // check application name
    cy.get('.example-app-name').contains('AnimalsCompany').should('be.visible')

    // click on 'Pracownicy' button
    cy.get(':nth-child(2) > :nth-child(1) > .btn').click()

    //click 'Dodaj pracownika' button
    cy.get('.border > .btn').click()

    // Fill 'Imię, Nazwisko, Departament, E-mail, Numer telefonu' input text
    cy.get('#firstname').should('be.visible').type('Jan')
    cy.get('#lastname').should('be.visible').type('Kowalski')
    cy.get('#departament').should('be.visible').type('R&D')
    cy.get('#email').should('be.visible').type('test@test.pl')
    cy.get('#numberphone').should('be.visible').type('48123456789')

    // Click 'Zapisz' button
    cy.get('.border > .btn-info').should('be.visible').click()

    // Check does above entry is on table of results
    cy.get(':nth-child(3) > .cdk-column-firstname').should('be.visible').contains('Jan')
    cy.get(':nth-child(3) > .cdk-column-lastname').should('be.visible').contains('Kowalski')
    cy.get(':nth-child(3) > .cdk-column-department').should('be.visible').contains('R&D')
    cy.get(':nth-child(3) > .cdk-column-email').should('be.visible').contains('test@test.pl')
    cy.get(':nth-child(3) > .cdk-column-numberphone').should('be.visible').contains('48123456789')

    // Check does also above entry has rest elements eg. operation buttons
    cy.get(':nth-child(3) > .cdk-column-Action-Buttons').should('be.visible').contains('Szczegóły')
    cy.get(':nth-child(3) > .cdk-column-Action-Buttons').should('be.visible').contains('Edytuj')
    cy.get(':nth-child(3) > .cdk-column-Action-Buttons').should('be.visible').contains('Usuń')

    // Choose 'Umowe o prace' dropdown option
    cy.get('.custom-select').should('have.value', '===')
    cy.get('select[formControlName=dropdownValue]').first().select('Umowa o prace')
    cy.get('.custom-select').should('have.value', 'Umowa o prace')

});

it('tests a pdf', () => {
    cy.task('getPdfContent', 'mypdf.pdf').then(content => {
        // test you pdf content here, with expect(this and that)...
    })
})

