CREATE TABLE Employees (
                      id INT AUTO_INCREMENT  PRIMARY KEY,
                      firstname VARCHAR(250) NOT NULL,
                      lastname VARCHAR(250) NOT NULL,
                      department VARCHAR(250) NOT NULL,
                      email VARCHAR(250) NOT NULL,
                      numberphone VARCHAR(250) NOT NULL
)

CREATE TABLE Animals (
                      id INT AUTO_INCREMENT  PRIMARY KEY,
                      ksywka VARCHAR(250) NOT NULL,
                      rodzaj VARCHAR(250) NOT NULL,
                      rasa VARCHAR(250) NOT NULL,
                      dataPrzyjecia VARCHAR(250) NOT NULL,
                      opis VARCHAR(250) NOT NULL
)

