package com.example.demo.Animal;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/animal")
@RestController()
public class AnimalController {

    @Autowired
    public AnimalService service;

    @GetMapping("")
    public ResponseEntity<Iterable<AnimalDao>> index()
    {
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping("/addnew")
    public void addAnimal(@RequestBody AnimalDao user){
        service.save(user);
    }

    @PutMapping("/{id}/edit")
    public void editAnimal(@PathVariable("id") Integer id, @RequestBody AnimalDao user) {
        service.save(user);
    }

    @DeleteMapping("/{id}/delete")
    public void deleteAnimal(@PathVariable("id") int id){
        service.deleteById(id);
    }

    @GetMapping(value = "/report/**")
    public void getGETExportReport(){

    }

    @PostMapping("/report//**")
    public void aloowAllReportRequests(){

    }

    /*  @Consumes(MediaType.APPLICATION_JSON)
      @Produces(MediaType.APPLICATION_PDF)*/
    @PostMapping(value = "/report/{filename}")
    public void getPOSTexportReport(@RequestBody AnimalDao animalDao,
                                    @PathVariable("filename") String filename,
                                    HttpServletResponse response) throws IOException, JRException {
        List<AnimalDao> animals = new ArrayList<>();
        animals.add(new AnimalDao("ksywka","rodzaj","rasa","dataPrzyjecia","opis"));
        //load file and compile it
        File file = ResourceUtils.getFile("src/main/resources/reports/animals/"+filename+".jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(animals);

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("ksywka", animalDao.getKsywka());
        parameters.put("rodzaj", animalDao.getRodzaj());
        parameters.put("rasa", animalDao.getRasa());
        parameters.put("dataPrzyjecia", animalDao.getDataPrzyjecia());
        parameters.put("opis", animalDao.getOpis());

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        String attachment = "attachment; filename="+filename+".pdf";
        response.setHeader("Content-Disposition", attachment);
        response.setHeader("Content-Type","application/pdf");
        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());

    }
}
