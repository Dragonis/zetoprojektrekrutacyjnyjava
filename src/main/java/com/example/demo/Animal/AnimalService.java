package com.example.demo.Animal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AnimalService {

    private AnimalRepository repo;

    @Autowired
    public AnimalService(AnimalRepository repo) {
        this.repo = repo;
    }

    public Optional<AnimalDao> findById(int id) {
        return repo.findById(id);
    }

    public Iterable<AnimalDao> findAll() {
        return repo.findAll();
    }

    public AnimalDao save(AnimalDao AnimalDao) {
        return repo.save(AnimalDao);
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB() {
        save(new AnimalDao("Pimpek","Pies","Labrador","01.01.2010","Opis"));
        save(new AnimalDao("Ksywka","Rodzaj","Rasa","DataPrzyjecia","Opis"));
    }
}
