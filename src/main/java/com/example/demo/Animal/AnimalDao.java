package com.example.demo.Animal;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Animals")
public class AnimalDao {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int id;

    private String ksywka;
    private String rodzaj;
    private String rasa;
    private String dataPrzyjecia;
    private String opis;

    public AnimalDao(String ksywka, String rodzaj, String rasa, String dataPrzyjecia, String opis){
        this.ksywka = ksywka;
        this.rodzaj = rodzaj;
        this.rasa = rasa;
        this.dataPrzyjecia = dataPrzyjecia;
        this.opis = opis;
    }
}
