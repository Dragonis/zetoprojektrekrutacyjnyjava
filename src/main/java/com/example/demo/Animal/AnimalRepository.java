package com.example.demo.Animal;

import org.springframework.data.repository.CrudRepository;

public interface AnimalRepository extends CrudRepository<AnimalDao, Integer> {

}
