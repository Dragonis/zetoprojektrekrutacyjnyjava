package com.example.demo.Employee;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity(name = "Emploees")
public class EmployeeDao {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int id;

    private String firstname, lastname, department, numberphone, email;

    public EmployeeDao(String firstname, String lastname, String department, String numberphone, String email) {
        this.firstname = firstname; this.lastname = lastname; this.department = department; this.numberphone = numberphone; this.email = email;
    }

  /*  private String firstname, lastname, department, numberphone, email,
            contractDate, nameCompanyOwner, surnameCompanyOwner, homeAdress,
            startWork, endWork, companyName, dateDocumentSigned,  kindOfWork,
            placeOfWork, workingHours, salary, dateToStartWork;

    public EmployeeDao(String firstname, String lastname, String department, String numberphone, String email,
                       String contractDate, String nameCompanyOwner, String surnameCompanyOwner, String homeAdress,
                       String startWork, String endWork, String companyName, String dateDocumentSigned,  String kindOfWork,
                       String placeOfWork, String workingHours, String salary, String dateToStartWork) {
        this.firstname = firstname; this.lastname = lastname; this.department = department; this.numberphone = numberphone; this.email = email;
        this.contractDate = contractDate; this.nameCompanyOwner = nameCompanyOwner; this.surnameCompanyOwner = surnameCompanyOwner; this.homeAdress = homeAdress;
        this.startWork = startWork; this.endWork = endWork; this.companyName = companyName; this.dateDocumentSigned = dateDocumentSigned; this.kindOfWork = kindOfWork;
        this.placeOfWork = placeOfWork; this.workingHours = workingHours; this.salary = salary; this.dateToStartWork = dateToStartWork;
    }*/


}
