package com.example.demo.Employee;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/employee")
@RestController()
public class EmployeeController {

    @Autowired
    private EmployeeService service;

    @GetMapping("")
    public ResponseEntity<Iterable<EmployeeDao>> index()
    {
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping("/addnew")
    public void addEmployee(@RequestBody EmployeeDao user){
        service.save(user);
    }

    @PutMapping("/{id}/edit")
    public void editEmployee(@PathVariable("id") Integer id, @RequestBody EmployeeDao user) {
        service.save(user);
    }

    @DeleteMapping("/{id}/delete")
    public void deleteEmployee(@PathVariable("id") int id){
        service.deleteById(id);
    }

 /*   @GetMapping(value = "/report}")
    public @ResponseBody byte[] report(HttpServletResponse response, @PathVariable("id") Integer id) throws Exception {

        final String logo_path = "/jasper/images/stackextend-logo.png";
        final String invoice_template = "/jasper/invoice_template.jrxml";

        Invoice invoice = invoiceRepo.getOne(id);

        File pdfFile = File.createTempFile("invoice", ".pdf");

        log.info(String.format("Invoice pdf path : %s", pdfFile.getAbsolutePath()));

        final Map<String, Object> parameters = new HashMap<>();

        parameters.put("logo", getClass().getResourceAsStream(logo_path));
        parameters.put("invoice", invoice);

        try (FileOutputStream pos = new FileOutputStream(pdfFile)) {
            // Load invoice jrxml template.
            final JasperReport report = jrxmlTemplateLoader.loadTemplate(invoice_template);

            // Create parameters map.
            // final Map<String, Object> parameters = parameters(invoice);

            // Create an empty datasource.
            final JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(
                    Collections.singletonList("Invoice"));

            // Render as PDF.
            JasperReportsUtils.renderAsPdf(report, parameters, dataSource, pos);

        } catch (final Exception e) {
            log.error(String.format("An error occured during PDF creation: %s", e));
        }

        byte[] bytes = Files.readAllBytes(Paths.get(pdfFile.getAbsolutePath()));

        return bytes;
    }*/

    @GetMapping(value = "/report/**")
    public void getGETExportReport(){

    }


    @PostMapping("/report/**")
    public void aloowAllReportRequests(){

    }


  /*  @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_PDF)*/
    @PostMapping(value = "/report/{filename}")
    public void getPOSTexportReport(@RequestBody EmployeeDao employeeDao,
                             @PathVariable("filename") String filename,
                             HttpServletResponse response) throws IOException, JRException {
        List<EmployeeDao> employees = new ArrayList<>();
        employees.add(new EmployeeDao("firstname","lastname","departament","number phone","email"));
        //load file and compile it
        File file = ResourceUtils.getFile("main/resources/reports/employees/"+filename+".jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(employees);

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("firstname", employeeDao.getFirstname());
        parameters.put("lastname", employeeDao.getLastname());
        parameters.put("department", employeeDao.getDepartment());
        parameters.put("numberphone", employeeDao.getNumberphone());
        parameters.put("email", employeeDao.getEmail());

        // 11 dodatkowych  zmiennych dla uop okr prob
        parameters.put("dateDocumentSigned","[zmienna1]");
        parameters.put("companyName","[zmienna2]");
        parameters.put("nameCompanyOwner","[zmienna3]");
        parameters.put("surnameCompanyOwner","[zmienna4]");
        parameters.put("homeAdress","[zmienna5]");
        parameters.put("startWork","[zmienna6]");
        parameters.put("kindOfWork",employeeDao.getDepartment());
        parameters.put("placeOfWork","[zmienna7]");
        parameters.put("workingHours","[zmienna8]");
        parameters.put("salary","[zmienna9]");
        parameters.put("dateToStartWork","[zmienna10]");

      /*  parameters.put("dateDocumentSigned",employeeDao.getDateDocumentSigned());
        parameters.put("companyName",employeeDao.getCompanyName());
        parameters.put("nameCompanyOwner",employeeDao.getNameCompanyOwner());
        parameters.put("surnameCompanyOwner",employeeDao.getSurnameCompanyOwner());
        parameters.put("homeAdress",employeeDao.getHomeAdress());
        parameters.put("startWork",employeeDao.getStartWork());
        parameters.put("kindOfWork",employeeDao.getKindOfWork());
        parameters.put("placeOfWork",employeeDao.getPlaceOfWork());
        parameters.put("workingHours",employeeDao.getWorkingHours());
        parameters.put("salary",employeeDao.getSalary());
        parameters.put("dateToStartWork",employeeDao.getDateToStartWork());*/

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        String attachment = "attachment; filename="+filename+".pdf";
        response.setHeader("Content-Disposition", attachment);
        response.setHeader("Content-Type","application/pdf");
        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());

    }
}
