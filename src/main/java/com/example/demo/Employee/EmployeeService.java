package com.example.demo.Employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class EmployeeService {

    private EmployeeRepository repo;

    @Autowired
    public EmployeeService(EmployeeRepository repo) {
        this.repo = repo;
    }

    public Optional<EmployeeDao> findById(int id) {
        return repo.findById(id);
    }

    public Iterable<EmployeeDao> findAll() {
        return repo.findAll();
    }

    public EmployeeDao save(EmployeeDao EmployeeDao) {
        return repo.save(EmployeeDao);
    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB() {
        save(new EmployeeDao("Jan",
                "Kowalski",
                "Dział księgowy",
                "48 739 854 621",
                "jan.kowalski@logo.pl"
                ));
        save(new EmployeeDao("Anna",
                "Nowak",
                "Dział sprzedaży",
                "15 822 64 79",
                "anna.nowak@logo.pl"
                ));
    }
}

